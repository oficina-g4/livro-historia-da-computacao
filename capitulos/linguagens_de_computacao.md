# 4-Liguagens de Computação e aplicações

Neste capitulo, iremos falar brevemente sobre as linguagens de computação. Iremos comentar sobre o que mudou nelas nesse tempo, desde suas origens, quais seus tipos e aplicações, as mais usadas e porquê de serem escolhidas, o porquê de terem sido criadas. Iremos falar também sobre a linguagem de máquina e por aí vai.

Mas afinal, o que são e para que servem as linguagens de programação? As linguagens de programação, assim como as linguagens orais e escritas que conhecemos, são linguagens formais, cada uma com suas características próprias e voltadas para certas aplicações, desde os softwares que estão no seu micro-ondas, celular, computador, vídeo game, um carro, um foguete e os números são incontáveis. O programador que tem domínio sobre as peculiaridades de determinada linguagem, naturalmente poderá criar programas(softwares) mais elaborados, mais eficientes e com mais recursos.

Muitas vezes, o mesmo programador precisa dominar várias linguagens, como C++ para criar um hardware e SQL para a base de dados, por exemplo. Então, um programador é como se fosse um escritor, criando sua própria história, que seria seu software, uma analogia meio boba porém com fundamento, um estudo publicado por Scientific Reports, um jornal de acesso aberto da Nature Publishing Group, indicou que  as pessoas com mais aptidão para programar são aquelas com fortes conhecimentos em linguagem, indicando que esse conhecimento pode ser ainda mais importante para um programador do que conhecimentos matematicos. Bom, é claro que aqueles com uma base forte tanto em linguagem quanto em matemática se saíram ainda melhor. 

Então, já sabemos um pouco sobre as linguagens em programação num geral. No próximo capitulo entenderemos um pouco sobre a linguagem de máquina.


fontes https://universidadedatecnologia.com.br/o-que-e-linguagem-de-programacao/

Journal Reference: Chantel S. Prat, Tara M. Madhyastha, Malayka J. Mottarella, Chu-Hsuan Kuo. Relating Natural Language Aptitude to Individual Differences in Learning Programming Languages. Scientific Reports, 2020; 10 (1) DOI: 10.1038/s41598-020-60661-8

 ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀[PÁGINA ANTERIOR](capitulos/a_internet.md)⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀          ⠀⠀⠀⠀⠀⠀[PRÓXIMA PÁGINA](capitulos/linguagem_de_maquina.md)
