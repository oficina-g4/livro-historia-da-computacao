# 5.1-Sistemas de informação

Nas palavras da instituição Unigranrio, "Via de regra, o conceito de Sistema de Informação é aplicável a todo mecanismo projetado com a finalidade de coletar, processar, armazenar e transmitir informações, de maneira a facilitar o acesso de usuários interessados, solucionando problemas e atendendo suas necessidades.". Basicamente, a area de sistemas de informação está diretamente ligada a infraestrutura de rede e processos de uma empresa assim com a centralização de dados. Ou seja, o profissional de sistemas de informação está mais ligado ao gerenciamento e  bom funcionanamento de todo o sistema de TI da sua empresa do que com o desenvolvimento, que seria o caso do engenheiro e cientista da computação.

![](http://www.tce.pi.gov.br/wp-content/uploads/2019/01/banner_analise_e_des_sistemas.jpeg)


fontes
https://portal.unigranrio.edu.br/blog/o-que-e-sistemas-de-informacao
https://canaltech.com.br/cursos/ciencia-e-engenharia-da-computacao-quais-as-diferencas-e-qual-escolher-153977/



⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀[PÁGINA ANTERIOR](capitulos/as_areas_de_ciencias_da_computacao.md)⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀[PRÓXIMA PÁGINA](capitulos/ciencia_da_computacao.md)
