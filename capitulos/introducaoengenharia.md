# Origem Exata Da Palavra Engenharia
A origem exata da palavra “Engenharia” vem da época em que os humanos se aplicavam a invenções habilidosas. Com invenções de dispositivos como a polia, a roda e as alavancas. A palavra engenheiro tem sua raiz na palavra “motor”, que vem do latim ingenium, que por fim, significa “qualidade inata, particularmente da força mental.” Por fim a palavra Engenheiro surgiu como uma pessoa que cria invenções práticas e bacanas.
## Como O Engenheiro É Descrito No Mercado
Hoje o engenheiro é descrito como alguém que adquiriu e está praticando seu conhecimento científico e técnico para projetar e construir soluções úteis. Tudo isso é realizado tendo em mente a funcionalidade, a economia operacional e à segurança a vida e à propriedade. 
### Evolução Da Engenharia 
A engenharia tradicional era dividida em áreas específicas, tais como Engenharia Civil, Engenharia Química, Engenharia Elétrica, Engenharia Aeroespacial e Engenharia Mecânica. No entanto, como o ser humano tem avançado rapidamente com a tecnologia novos ramos da engenharia foram desenvolvidos. E podem ser encontrados nos campos, Engenharia da computação, Engenharia de software, Engenharia Molecular, e muito mais. 

⠀[PÁGINA ANTERIOR](capitulos/introducao.md)⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀          [PRÓXIMA PÁGINA](capitulos/evolucao_dos_computadores.md)
