## 4.1-Linguagem de Máquina
        
Antes de iniciarmos nossa discussão a respeito das linguagens de programação, precisamos entender o porquê se haver surgido a necessidade de sua criação. E é por isso que falaremos sobre as linguagens de máquina.
Os computadores, desde sua criação até os dias atuais, só entendem duas "palavras", 0(zero) e 1(um). esses são os unicos dados que os computadores entendem. Claro, isso também não passa de uma abstração do que está acontecendo dentro do computador, que interpreta duas tensões discretas chamadas de zeros e um. Tudo o que você pode ver, seja na tela do seu computador, no seu celular ou tablet, geladeira, microondas, e qualquer equipamento eletrônico em geral, não passa de um processador lendo sequencias gigantescas de zeros e uns e traduzindo para isso que você pode ler agora.
A seguir, um pequeno exemplo do que isso que foi dito acima o pode parecer:

``` c
0 1 0 0 1 0 0 0  0 1 1 0 0 1 0 1
0 1 1 0 1 1 0 0  0 1 1 0 1 1 0 0
0 1 1 0 1 1 1 1  0 0 1 0 0 0 0 0
0 1 0 1 0 1 1 1  0 1 1 0 1 1 1 1
0 1 1 1 0 0 1 0  0 1 1 0 1 1 0 0
0 1 1 0 0 1 0 0  0 0 1 0 0 0 0 1
```
figura 4.1

Esse conjunto de zeros e uns acima é o que chamamos de linguagem de máquina. você poderia dizer o que está escrito ali? Exato, na verdade esse numeros por si só não querem dizer nada, porém, os computadores vêm pre programados para entender certas sequencias de 0s e 1s, convertendo-os para letras, numeros, simbolos e até mesmo emojis. Mas afinal, o que quer dizer esse conjunto de 96 digitos? Todos esses números são necessários para o computador nos devolver a palavra _Hello world!_ . Vamos a seguir entender um pouco mais sobre numeros binarios.

### Números binários

Estamos muito acostumados com os numerais decimais Ex.: 13, 0, 1038, 0.97, 1/4, 3², π ... mas afinal, você pode dizer as características dele e porquê tem esse nome? seu nome, decimal, indica que o número está na base 10, ou seja, para cada algorismo de um número, multiplicamos ele por uma potência de 10, que vai de menos infinito a mais infinito. Por exemplo, no número 123 nós temos os algarismos 3, 2 e 1, cada um multiplicado pela sua respectiva potência de base 10. Nesse casos, `3 * 10° + 2 * 10¹ + 1 * 10²` que, como sabemos, `resulta em 3 + 20 + 100 = 123`. Como temos 10 algarismos para trabalhar, de  0 a 9, uma vez que chegue em 9, acrescentamos um a próxima potência de 10.
Nessa perspectiva, o que diferencia os algarismos binários, é que temos apenas 2 algarismos, 0 e 1, trabalhando assim na base 2. Seguindo essa lógica, quanto valeria o seguinte número: 01001000? Se você respondeu 72, meus parabéns, mas se não conseguiu, vamos explicar com exemplos a seguir.
Seguindo a lógica dos numerais decimais, temos que cada um dos números são multiplicados por uma potência de 2, partindo do zero. Iremos utilizar x^(n) para indicar uma potÊncia, sendo x a base e n o expoente. 
Assim, temos que 01001000 é igual a:
``` 
0 * 2^(7) + 1 * 2^(6) + 0 * 2^(5) + 0 * 2^(4) + 1 * 2^(3) + 0 * 2^(2) + 0 * 2^(1) + 1 * 2^(6)
```
Como sabemos que todo número multiplicado por 0 é igual a 0. nos resta apenas:
``` 
1 * 2^(6) + 1 * 2^(3)
```
Que é igual a:
``` 
64 + 8 = 72
```
Embora pareça complicado a princípio, é ainda mais simples que nossos queridos decimais.

Você notou, esse número é igual a primeira sequência de 8 digitos que utilizamos na figura 4.1. Esse mesmo número, 72, é configurado em nossas máquinas segundo a tabela ascii, e, nessa tabela, o número 72 equivale ao H maiúsculo. Interessante não? Isso encerra nossa pequena introdução à linguagem de máquina, e agora partiremos para as linguagens de programação um pouco mais próximas do que são atualmente, as linguagens de baixo nível.



⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀[PÁGINA ANTERIOR](capitulos/linguagens_de_computacao.md)⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀          ⠀⠀⠀⠀⠀⠀[PRÓXIMA PÁGINA](capitulos/linguagem_de_baixo_nivel.md)
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀

