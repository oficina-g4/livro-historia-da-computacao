# 4.3-Linguagens de alto nível

E meus caros nerds, cá estamos nas linguagens de alto nível, as mais próximas da nossa linguagem, e para começar, iremos mostrar um exemplo de hello world em C para ilustrar.

```c
#include <stdio.h> // Uma biblioteca com varios codigos prontos que vc pode chamar e usar

int main(void)
{
    printf("Hello, World!"); // Nossa função principal, Hello, World!
}
```

A linguagem C, ainda que antiga, já se aproximando de seu 50th aniversário, ainda é amplamente utilizada até hoje, junto de suas variantes, C++ e C#, sendo essas duas ultimas orientadas a objeto, e a primeira estrutural. As linguagens de alto nível, estando mais proximas da nossa linguagem escrita se tornam mais fáceis de aprender em comparação com assembly, entre outras linguagens de baixo nível. E é basicamente por isso que utilizamos elas, mas é claro que não se limita apenas a isso.

Um ponto interessante em C, é que podemos trabalhar com assembly nele, também, claro que de forma limitada, mas isso de deve ao fato da linguagem C ser construida em assembly. Ou seja, antes de compilar nosso código para que nosso computador entenda, ele é primeiro convertido em assembly, e posteriormente para linguagem de máquina. 

Até aqui, talvez você tenha se perguntado, porque temos tantas linguagens para fazer uma mesma função, que é passar instruções à máquinas? Vamos usar um exemplo cotidiano, você vai à padaria e pede pão ou dependendo da região que estiver, um cacetinho. Dois termos para a mesma coisa. Claro, esse é um exemplo muito bobo, mas dá pra entender a ideia. Além disso, as linguagens são especializadas a funções, e algumas se destacam mais do que as outras para determinadas tarefas. Por exemplo, para desenvolvimento web, temos html, css e javascript. Para jogos digitais comumente vemos C++, C#, e java. Para inteligencia artificial temos python, para banco de dados temos SQL. Embora várias dessas linguagens possam ser usadas em outras áreas, é em algumas poucas que elas brilham, e entender qual linguagem usar para determinada ocasião vai te poupar muita dor de cabeça.

Até agora, falamos muito sobre linguagens de computação, mas afinal, elas são mesmo tão importantes assim? Bom, é claro que sim, mas geralmente quem está começando a programar se preocupa tanto com a linguagem, que acaba esquecendo das partes mais importante, que é o desenvolvimento do raciocínio lógico, habilidade para desenvolver algoritmos eficientes e criatividade. ma vez que você obtenha domínio sobre certas habilidades, como as supracitadas, aprender uma linguagem para adicionar ao seu arsenal não será dificíl. Programadores são resolvedores de problemas, não poetas. Aqui encerramos nossa conversa sobre linguagens de computação. 

```c
#include <stdio.h> 

int main(void)
{
    printf("Vejo vocês no próximo capítulo!");
}
```




https://becode.com.br/linguagens-alto-nivel-x-baixo-nivel/


⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀[PÁGINA ANTERIOR](capitulos/linguagem_de_baixo_nivel.md)⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀[PRÓXIMO CAPÍTULO](capitulos/as_areas_de_ciencias_da_computacao.md)
