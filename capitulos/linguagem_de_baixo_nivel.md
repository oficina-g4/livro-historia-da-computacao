# 4.2-Linguagens de Baixo Nível

Como você pode já ter percebido, ou não, programar com números binários era muito trabalhoso, e essa dificuldade fez com que os programadores criassem as primeiras linguagens de programação, que conhecemos agora como linguagens de baixo nível e são pouco utilizadas, embora ainda se use em sistemas mais antigos e outras aplicações; e, quando falamos em linguagem de baixo nível, uma das mais importantes, se não a mais, é a linguagem chamada Assembly.

Essa linguagem consiste em simbolos equivalentes a linguagem de máquina específicos de certos computadores. Computadores de fabricantes diferentes necessitam de diferentes linguagens em assembly. Basicamente, para programar em assembly, o programador necessita ter conhecimentos profundos sobre a arquitetura da máquina em questão, uma vez que você irá trabalhar com entradas e saídas, memória, comunicação, controle e processamento.

Ufa! Como vocês podem ver, programar em assembly é um tanto complicado, porém, muito mais fácil que mexer com zeros e uns. Bom, na verdade, o que acontece é que o código em assembly converte essas instruções com linguagens mais próximas da nossa escrita, para zeros e uns, ou seja, para binário. O computador continua só entendendo 0 e 1, mas interagir com ele a partir dela fica muito mais fácil.

### Abaixo, segue-se um exemplo de codigo que poderiamos usar para escrever um hello world em assembly:

```c
; hello-DOS.asm - single-segment, 16-bit "hello world" program
;
; assemble with "nasm -f bin -o hi.com hello-DOS.asm"

    org  0x100        ; .com files always start 256 bytes into the segment

    ; int 21h is going to want...

    mov  dx, msg      ; the address of or message in dx
    mov  ah, 9        ; ah=9 - "print string" sub-function
    int  0x21         ; call dos services

    mov  ah, 0x4c     ; "terminate program" sub-function
    int  0x21         ; call dos services

    msg  db 'Hello, World!', 0x0d, 0x0a, '$'   ; $-terminated message
```

No bloco acima, vocês podem perceber que essa linguagem é muito mais próxima do que estamos acostumados, comandos como mov, int, org, entre outros, embora em inglês.

Assim como dissemos mais cedo, assemly ainda tem suas importÂncias no mundo dos códigos, um dos exemplos que podemmos usar é na detecção de malwares, vírus, assim como alguns códigos procedurais que podem ter melhor desempenho se programados em assembly.

Assembly também é chamada de linguagem estruturada, ou seja, ela é voltada a procedimentos e funções desejadas pelo usuário, de matemática até onde sua imaginação puder levar. Atualmente temos as conhecidas linguagens orientadas a objeto, como C, C++, Java, Python, e a lista não para de crescer, mas isso é assunto do nosso próximo capítulo, Linguagens de alto nível.










fontes: 

https://www.ic.unicamp.br/~pannain/mc404/aulas/pdfs/Art%20Of%20Intel%20x86%20Assembly.pdf
https://montcs.bloomu.edu/Information/LowLevel/Assembly/hello-asm.html
https://www.britannica.com/technology/assembly-language
https://www.devmedia.com.br/programacao-orientada-a-objetos-e-programacao-estruturada/32813#:~:text=Portanto%2C%20como%20vimos%20no%20decorrer,e%20fun%C3%A7%C3%B5es%20definidas%20pelo%20usu%C3%A1rio.
https://minutodaseguranca.blog.br/importancia-da-linguem-de-programacao-de-baixo-nivel/#:~:text=O%20Assembly%20continua%20sendo%20ferramenta,atualizadas%20como%20os%20pr%C3%B3prios%20invasores.

⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀[PÁGINA ANTERIOR](capitulos/linguagem_de_maquina.md)⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀[PRÓXIMA PÁGINA](capitulos/linguagem_de_alto_nivel.md)
