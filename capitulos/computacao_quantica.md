# 6.1-Computador Quântico 
é uma máquina que executa seu processamento baseado nas propriedades quânticas da matéria, criando um novo e revolucionário modo de armazenar e tratar dados.
A teoria do Computador Quântico dentro da ciência da computação existia desde a época de Albert Einstein, mas somente nos anos 80 foram feitas as primeiras tentativas de se construir algo semelhante ao magnífico computador quântico.
## O Uso das Propriedades Quânticas pela Ciência da Computação
O mundo quântico é um mundo estranho para os leigos, porém o mesmo já vem sendo explorado com êxito pela ciência da computação.

A principal propriedade quântica que está sendo estudada, sendo esta a base do computador quântico, é a chamada Propriedade da Sobreposição.

Esta propriedade não existe no mundo macroscópico (mundo visível a olho nu pelos humanos). Tal propriedade define que um elétron pode girar para a esquerda, girar para a direita ou então girar para os dois lados simultaneamente, gerando 3 estados possíveis e diferentes.

Uma analogia ao mundo real seria: é como se um copo de água tivesse a possibilidade de estar cheio, vazio ou então cheio e vazio ao mesmo tempo!

Dentro do escopo da ciência da computação, este fato define que através das propriedades dos elétrons, podemos criar um bit de 3 estados, ao invés do bit tradicional que tem 2 estados (0 ou 1).

Este bit mais poderoso, foi denominado pelos especialistas em ciência da computação como Bit Quântico (qubit).
### Capacidade de Processamento
Levando em conta o processamento dentro da ciência da computação, o computador quântico é mais eficiente pois trabalha com uma quantidade mais densa de informações ao mesmo tempo.

Alguns problemas que um computador eletrônico levaria milhares de anos para resolver, um computador quântico seria capaz de resolver em alguns minutos.

https://www.guiadacarreira.com.br/profissao/computador-quantico-ciencia-computacao/


⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀[PÁGINA ANTERIOR](capitulos/futuro_da_computacao.md)⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀          ⠀⠀⠀⠀⠀⠀[PRÓXIMA PÁGINA](capitulos/machine_learning.md)
