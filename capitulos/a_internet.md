# A internet

Ah... a internet. Atualmente é dificil de imaginar como seria a vida sem a internet, ela está tão associada a nossas vidas como se fosse algo normal, e realmente é, mas nem sempre foi assim. A internet foi criada em 1969 com a internção de ligar laboratórios de pesquisa, antes de chegar a escala que conhecemos hoje e, na área tecnologica, geralmente é assim que as coisas nascem, outro exemplo masi recent que temos é o facebook, que foi criado como uma rede social exclusiva de harvard até posteriormente virar global. 

Quando falamos de internet, estamos falando sobre uma rede de redes compartilhadas. O acesso à internet de modo externo se dá quando a sua rede local se conecta a outra rede maior, no caso, o seu provedor de internet, por meio da tecnologia TCP/IP, um modo de comunicação baseado no endereço de IP (Internet Protocol).

Bom, de forma geral a internet é essencial para nossa vida a ponto de ser considerada um direito humano.

![](https://www.caminhosdoemprego.com/wp-content/uploads/2014/10/internet.jpg)

https://webfoundation.org/about/vision/history-of-the-web/?gclid=Cj0KCQjwvYSEBhDjARIsAJMn0lhrGKGIPS6-688tbhTViUzEPjxJgCq1hqkWVe-prJVETu2xOHAtexwaAiNHEALw_wcB

[PÁGINA ANTERIOR](capitulos/primeiros_computadores.md)⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀                  [PRÓXIMA PÁGINA](capitulos/linguagens_de_computacao.md)
