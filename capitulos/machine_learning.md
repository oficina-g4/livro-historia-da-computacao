# Machine learning

Neste capitulo iremos falar brevemente sobre machine learning, o que é e quais suas aplicações.

Machine learning é um tipo de [inteligência artificial](capitulos/inteligencia_artificial.md) que é de certa forma controlada. Existem 3 tipos de machine learning: Supervised learning, unsupervised learning e reinforcement learning. de certa forma, machine learning aprende algoritmos na base da tentativa e erro. A versão supervisionada é aquela em que ocorre contato humano durante esse aprendizado, enquanto a não supervisionada é deixada por conta de maquina aprender sozinha, enquanto, por outro lado, a aprendizagem reforçada é supervisionada por um ambiente dinâmico. 

![](https://www.doutorecommerce.com.br/wp-content/uploads/2019/07/machine-learning.jpg)

https://www.internetsociety.org/resources/doc/2017/artificial-intelligence-and-machine-learning-policy-paper/?gclid=Cj0KCQjwvYSEBhDjARIsAJMn0ljIHQHp22fb9b1qeDsbX4hG2hRrUMp5VFSBCL-EGmIi7wd399K3MtUaAluxEALw_wcB

[PÁGINA ANTERIOR](capitulos/computacao_quantica.md)⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀          [PRÓXIMA PÁGINA](capitulos/inteligencia_artificial.md)
