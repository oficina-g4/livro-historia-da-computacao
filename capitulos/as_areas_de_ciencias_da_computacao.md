# 5-Áreas da computação

Se tem um tópico que deixa muitas pessoas interessadas em entrar na área da computação, é sobre qual curso elas deveriam escolher. De fato, é uma pergunta difícil, uma vez que todas as áreas estão ligadas umas as outras, mas cada uma delas tem suas particularidades como veremos nos capítulos a seguir. Começaremos falando sobre Sistemas de informação, então prosseguiremos para Ciência da Computação, até falarmos da Engenharia da Computação, até porque a melhor bolacha do pacote a gente deixa para o final.



Sem mais enrolação, vamos falar sobre Sistema de Informação.



⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀[PÁGINA ANTERIOR](capitulos/linguagem_de_alto_nivel.md)⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀[PRÓXIMA PÁGINA](capitulos/sistemas_de_informacao.md)
