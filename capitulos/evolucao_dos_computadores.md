A evolução dos computadores acompanhou a evolução da sociedade durante os séculos século XX e XXI. Entretanto, a história do computador não teve início apenas na modernidade. Lembre-se que os computadores são aparelhos eletrônicos que recebem, armazenam e produzem informações de maneira automática. Eles fazem parte do nosso cotidiano, sendo cada vez maior o número de computadores usados no mundo.

[PÁGINA ANTERIOR](capitulos/introducaoengenharia.md)⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀          [PRÓXIMA PÁGINA](capitulos/primeiros_computadores.md)
