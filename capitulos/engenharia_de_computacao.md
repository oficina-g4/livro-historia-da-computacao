# 5.3-Engenharia da computação

Agora falaremos sobre a graduação em engenharia da computação. A diferença fundamental entre a engenharia e a ciência da computação é que, diferente do curso de ciencias que é mais focado em software, a engenharia recebe formação tanto em software quando em hardware, ou seja, está diretamente ligada a criação de dispositivos eletrônicos, como celulares, computadores, carros e por aí vai.

A graduação em engenharia da computação é um hibrido entre ciência da computação e engenharia elétrica, e por isso acaba sendo uma graduação meio confusa, junto ao fato de ser relativamente recente. Após desenvolver uma boa base elétrica e computacional, o engenheiro da computação é capaz de produzir seus próprios dispositivos com as mais diversas aplicações.

O engenheiro de computação, durante sua formação passa pelas materias comuns da engenharia, como calculo, fisica e geometria analitica, a famosa trinca, de modo a desenvolver uma forte base matemática que irão ser essencias ao decorrer do curso e durante toda a sua carreira.

![](https://www.unicesumar.edu.br/blog/wp-content/uploads/2017/06/engenharia-software.jpg)


⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀[PÁGINA ANTERIOR](capitulos/ciencia_da_computacao.md)⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀          ⠀⠀⠀⠀⠀⠀[PRÓXIMO CAPÍTULO](capitulos/futuro_da_computacao.md)
