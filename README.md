# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Introdução](capitulos/introducao.md)
1. [A Engenharia](capitulos/introducaoengenharia.md)
1. [Evolução dos Computadores Pessoais e sua Interconexão](capitulos/evolucao_dos_computadores.md)
    - 3.1[Primeira Geração](capitulos/primeiros_computadores.md)
    - 3.2[A internet](capitulos/a_internet.md)
1. [Linguagens de computação e sua história](capitulos/linguagens_de_computacao.md)
    - 4.1[Linguagem de máquina](capitulos/linguagem_de_maquina.md)
    - 4.2[Linguagens de baixo nível](capitulos/linguagem_de_baixo_nivel.md)
    - 4.3[Linguagens de alto nível](capitulos/linguagem_de_alto_nivel.md)
1. [As áreas de ciencias da computação](capitulos/as_areas_de_ciencias_da_computacao.md)
    - 5.1[Sistemas de informação](capitulos/sistemas_de_informacao.md)
    - 5.2[Ciência da computação](capitulos/ciencia_da_computacao.md)
    - 5.3[Engenharia da Computação](capitulos/engenharia_de_computacao.md)
1. [O futuro da computação](capitulos/futuro_da_computacao.md)
    - 6.1[Computação quântica](capitulos/computacao_quantica.md)
    - 6.2[Machine Learning](capitulos/machine_learning.md)
    - 6.3[Inteligência Artificial](capitulos/inteligencia_artificial.md)


## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://assets.gitlab-static.net/uploads/-/system/user/avatar/8313095/avatar.png?width=400)  | Satil Pereira | satil_pereira | [satilpereira@utfpr.edu.br](mailto:satilpereira@utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8333209/avatar.png?width=400)  | Rafael Inácio| rafaelinacio829 | [rafaelinacio@alunos.utfpr.edu.br](mailto:rafaelinacio@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8279829/avatar.png?width=400)  | Fernando Gnoatto| fernando_gnoatto | [fernn65@hotmail.com](mailto:fernn65@hotmail.com)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8279941/avatar.png?width=400)  | Pedro Henrique| MisterPedro | [psantiago@aluno.utfpr.edu.br](mailto:psantiago@aluno.utfpr.edu.br)
